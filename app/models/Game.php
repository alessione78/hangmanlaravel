<?php
class Game extends Eloquent
{
	CONST STATUS_BUSY = 'busy';
	CONST STATUS_SUCCESS = 'success';
	CONST STATUS_FAIL = 'fail';

	protected $table = 'game';

	public $timestamps = false;

	protected $fillable = array('status', 'tries_left');

}