<?php
use Hangman\Storage\Game\EloquentGameRepository as Game;

class GameController extends BaseController
{
	private $game;
	
	/**
	* Automatic resolution for IoC
	*/
	public function __construct(Game $game) {
		$this->game = $game;
	}

	/**
	* Start new Game
	*/
	public function newGame()
	{
			
		Session::flush();

		$newGame = $this->game->create();
		
		$response = array(
			'id' => $newGame->id,
			'status' => $newGame->status,
			'word' => $this->game->resolveWordToAssert()
		);

		return Response::Json($response);
	}

	/**
	* Try to assert a letter
	* @param int $gameId
	*/

	public function assertLetter($gameId)
	{
		$letter = Input::get('letter');

		$game = $this->game->find($gameId);

		$hiddenWord = $this->game->resolveWordToAssert($letter);

		$response = array(
			'word' => $hiddenWord,
			'tries_left' => $game->tries_left,
			'status' => $game->status
		);

		return Response::Json($response);

	}

	
}