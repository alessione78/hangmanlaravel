<?php
class GameControllerTest extends TestCase
{

	private $gameMock;
	private $wordMock;

	public function setUp()
	{
		parent::setUp();
		$this->gameMock = $this->mock('Hangman\Storage\Game\EloquentGameRepository');
	}

	public function mock($class)
	{
	  	$mock = \Mockery::mock($class);

	  	$this->app->instance($class, $mock);
	  
  		return $mock;
	}

	public function testNewGame()
	{
		
		$this->gameMock->id = 1;
		$this->gameMock->status = 'busy';
		$this->gameMock->tries_left = 11;

		$this->gameMock
			 ->shouldReceive('create')
			 ->andReturn($this->gameMock);

		$this->gameMock
			 ->shouldReceive('save')
			 ->andReturn($this->gameMock);

		$this->gameMock
		 ->shouldReceive('resolveWordToAssert')
		 ->andReturn('t..t');


		$this->gameMock->word = 'test';
		
		$gameController = new GameController($this->gameMock);

		$response = $gameController->newGame();


		$this->assertTrue($response instanceof Illuminate\Http\JsonResponse);
	}


	/**
	* @depends testNewGame
	* @dataProvider letterProvider
	*/
	public function testAssertLetter($letterSet)
	{
		$this->gameMock->id = 1;
		$this->gameMock->tries_left = 11;
		$this->gameMock->word = 'test';
		$this->gameMock->status = 'busy';

		$this->gameMock
		 ->shouldReceive('resolveWordToAssert')
		 ->andReturn('t..t');

		$this->gameMock
			 ->shouldReceive('find')
			 ->andReturn($this->gameMock);

		$this->gameMock
			 ->shouldReceive('save')
			 ->andReturn($this->gameMock);


		foreach ($letterSet as $letters) {
			foreach($letters as $letter) {
				$gameController = new GameController($this->gameMock, $this->wordMock);

				$response = $gameController->assertLetter($this->gameMock->id, $letter);

				$this->assertTrue($response instanceof Illuminate\Http\JsonResponse);
			}
		}
	}

	public function letterProvider()
	{
		return array(
			array(
				array(
					array(
						'e',
						's',
					),
					array(
						'l',
						'y',
						'a',
						'm',
						'w',
						'd',
						'o',
						'p',
						'a',
						'g',
						'w',
						'd',
					),
					
				)
			)
		);
	}
	
}