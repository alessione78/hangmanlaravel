<?php

class GameTest extends TestCase {

	private $gameId;

	public function setUp()
	{
		parent::setUp();
	}

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testNewGameShouldReturnNewGame()
	{
		$crawler = $this->client->request('GET', '/games');

		$responseArray = json_decode($this->client->getResponse()->getContent(), true);

		$gameId = $responseArray['id'];

		$this->assertJson($this->client->getResponse()->getContent());
		$this->assertTrue($this->client->getResponse()->isOk());
		$this->assertArrayHasKey('id', $responseArray);
		$this->assertArrayHasKey('status', $responseArray);
		$this->assertArrayHasKey('word', $responseArray);
		
		$this->assertEquals($responseArray['status'], 'busy');

		return $gameId;
	}

	/**
	* 
	* @depends testNewGameShouldReturnNewGame
	* @dataProvider letterProvider
	*/

	public function testAssertLetterWithWrongLetterShouldFail($letter, $gameId)
	{
		
		$gameObj = Game::find($gameId);

		$wordToAssertArray = str_split($gameObj->word);
		$tries = array_diff($letter, $wordToAssertArray);

		foreach($tries as $char) {
			$crawler = $this->client->request('PUT', '/games/'.$gameId.'?letter='.$char);

			$response = $this->client->getResponse()->getContent();
			
			$responseArray = json_decode($response, true);

			if ($responseArray['status'] == 'success') {
				break;
			}

			$this->assertJson($response);
			$this->assertTrue($this->client->getResponse()->isOk());
			$this->assertArrayHasKey('tries_left', $responseArray);
			$this->assertArrayHasKey('status', $responseArray);
			$this->assertArrayHasKey('word', $responseArray);
		}

		$this->assertEquals($responseArray['status'], 'fail');
	}

	/**
	* 
	* @depends testNewGameShouldReturnNewGame
	* @dataProvider letterProvider
	*/
	public function testAssertLetterWithRightLetterShouldSucced($letter, $gameId)
	{
		
		$gameObj = Game::find($gameId);

		$wordToAssertArray = str_split($gameObj->word);

		$tries = array_intersect($letter, $wordToAssertArray);

		foreach($tries as $char) {
			$crawler = $this->client->request('PUT', '/games/'.$gameId.'?letter='.$char);

			$response = $this->client->getResponse()->getContent();
			
			$responseArray = json_decode($response, true);

			if ($responseArray['status'] == 'success') {
				break;
			}

			$this->assertJson($response);
			$this->assertTrue($this->client->getResponse()->isOk());
			$this->assertArrayHasKey('tries_left', $responseArray);
			$this->assertArrayHasKey('status', $responseArray);
			$this->assertArrayHasKey('word', $responseArray);
		}

		$this->assertEquals(strpos($responseArray['word'], "."), null);
		$this->assertEquals($responseArray['status'], 'success');
	}

	public function letterProvider()
	{
		return array(
			array(
				range('a', 'z'),
			)		
		);
	}

	public function testAssertLetterWithNoChar()
	{
		Route::enableFilters();

		$newGame = $this->client->request('GET', '/games');
		$newGameResponse = $this->client->getResponse()->getContent();

		$responseArray = json_decode($newGameResponse, true);

		$crawler = $this->client->request('PUT', '/games/'.$responseArray['id'].'?letter=2');

		$response = $this->client->getResponse()->getContent();
		
		$responseArray = json_decode($response, true);
		
		$this->assertJson($response);
		$this->assertTrue($this->client->getResponse()->isOk());
		
		$this->assertArrayHasKey('letter', $responseArray);
		$this->assertEquals($responseArray['letter'], 'invalid');
		
	}

}
