<?php
namespace Hangman\Storage;

use Illuminate\Support\ServiceProvider;
 
class StorageServiceProvider extends ServiceProvider {
 
  public function register()
  {
    $this->app->bind(
      'Hangman\Storage\Game\GameRepository',
      'Hangman\Storage\Game\EloquentGameRepository',
      'Hangman\Storage\Word\WordRepository',
      'Hangman\Storage\Word\EloquentWordRepository'
    );
  }
 
}