<?php
namespace Hangman\Storage\Word;

use Word;

class EloquentWordRepository implements WordRepository
{
	
	public function first() 
	{
		return Word::first();
	}

	public function orderByRaw($input)
	{
		return Word::orderByRaw($input);
	}
}