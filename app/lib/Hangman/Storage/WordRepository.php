<?php
namespace Hangman\Storage\Word;

interface WordRepository
{
	public function first();

	public function orderByRaw($input);

}