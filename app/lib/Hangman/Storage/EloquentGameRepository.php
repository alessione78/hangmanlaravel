<?php
namespace Hangman\Storage\Game;

use Game;
use Word;

class EloquentGameRepository implements GameRepository
{
	
	public function create($input = array())
	{
		$word = Word::orderByRaw('RAND()')->first();

		$this->game = Game::create($input);
		$this->game->word = $word->word;
		$this->game->status = Game::STATUS_BUSY;
		$this->save();

		return $this->game;
	}

	public function find($id)
	{
		$this->game = Game::find($id);

		return $this->game;
	}

	public function save()
	{
		return $this->game->save();
	}

	/*public function resolveWordToAssert($letter = null)
	{
		return $this->game->resolveWordToAssert($letter);
	}*/

	/**
	* Check if the letter is part of the word and store it in session.
	* @param object $game
	* @param string $letter
	*/
	public function resolveWordToAssert($letter = null)
	{
		$hideWordToAssertArray = str_split($this->game->word);
		
		if (in_array($letter, $hideWordToAssertArray)) {
			if (!in_array($letter, \Session::get('assertedLetters', array()))) {
				\Session::push('assertedLetters', $letter);	
			}
		} else {
			$this->reduceTriesLeft();
		}

		for($i = 1; $i < (count($hideWordToAssertArray) - 1); $i++) {
			if (!in_array($hideWordToAssertArray[$i], \Session::get('assertedLetters', array()))) {
				$hideWordToAssertArray[$i] = ".";
			}
		}

		if (!in_array(".", $hideWordToAssertArray)) {
			$this->game->update(array('status' => Game::STATUS_SUCCESS));
		}

		return join("",$hideWordToAssertArray);
	}


	/**
	* Reduce tries left in case of wrong assertion
	* @param object $game
	*/
	public function reduceTriesLeft()
	{

		if ($this->game->tries_left > 0) {
			$this->game->tries_left = ($this->game->tries_left - 1); 
			
			if ($this->game->tries_left == 0) {
				$this->game->status = Game::STATUS_FAIL;
			}
			
			$this->save();
		}
	}
}