<?php
namespace Hangman\Storage\Game;

interface GameRepository
{
	public function create();

	public function find($id);

	public function save();

}